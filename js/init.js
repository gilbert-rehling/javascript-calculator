/**
 * This script initialises the calculator app.
 * Sets up the config, which includes all the elements and boot styles.
 * Creates the container.
 * Initialise the container on the page.
 * Set the boot styles.
 */

/**
 * Initialise the base function for our prototyping.
 * The config will contain references for all the required elements to build the calculator and the styles for each element.
 * We'll use a basic 'keying' to tie elements together and make the setup as dynamic as possible.
 */
var $calculator = function () {
    "use strict";
    this.config = {
        "containers" : {
            "container"    : {"type" : "div", "level" : 0, "children" : ['screen', 'body']},
            "screen"       : {"type" : "div", "level" : 1, "children" : []},
            "body"         : {"type" : "div", "level" : 1, "children" : ['memories', 'buttons', 'functions']},
            "memories"     : {"type" : "div", "level" : 2, "children" : ['memory']},
            "memory"       : {"type" : "span", "level" : 3, "children" : [], "count" : 5, "function" : 'setMemories'},
            "buttons"      : {"type" : "div", "level" : 2, "children" : ['button']},
            "button"       : {"type" : "span", "level" : 3, "children" : [], "count" : 11, "function" : 'setButtons'},
            "functions"    : {"type" : "div", "level" : 2, "children" : ['function']},
            "function"     : {"type" : "span", "level" : 3, "children" : [], "count" : 5, "function" : 'setFunctions'}
        },
        "styles" : {
            "container" : {
                "width"             : '350px',
                "margin"            : '0 auto',
                "color"             : '#000000',
                "border-color"      : '#000080',
                "border-width"      : '2px',
                "border-style"      : 'solid',
                "background-color"  : '#878787',
                "min-height"        : '150px'
            },
            "screen" : {
                "width"             : '99%',
                "color"             : '#ffffff',
                "border-color"      : '#666666',
                "border-width"      : '2px',
                "border-style"      : 'solid',
                "background-color"  : '#000000',
                "height"            : '55px',
                "text-align"        : 'center'
            },
            "body" : {
                "display"           : 'inline-block',
                "width"             : '99%',
                "position"          : 'relative',
                "border-color"      : '#666666',
                "border-width"      : '2px',
                "border-style"      : 'solid',
                "borderTopWidth"    : '0'
            },
            "memories" : {
                "width"             : '75%',
                "display"           : 'inline-block',
                "text-align"        : 'center',
                "border-color"      : '#000000',
                "border-width"      : '1px',
                "border-style"      : 'solid',
                "borderTopWidth"    : '0',
                "background-color"  : '#989898'
            },
            "memory" : {
                "display"           : 'inline-block',
                "width"             : '15%',
                "min-height"        : '26px',
                "margin"            : '5px',
                "color"             : '#ffffff',
                "border-color"      : '#000000',
                "border-width"      : '1px',
                "border-style"      : 'solid',
                "border-radius"     : '15px',
                "background-color"  : '#111111'
            },
            "buttons" : {
                "display"           : 'inline-block',
                "width"             : '75%',
                "border-color"      : '#000000',
                "border-width"      : '1px',
                "border-style"      : 'solid',
                "background-color"  : '#666666',
                "min-height"        : '60px',
                "text-align"        : 'center'
            },
            "button" : {
                "display"           : 'inline-block',
                "width"             : '25%',
                "min-height"        : '35px',
                "margin"            : '5px',
                "padding"           : '8px 0 0 0',
                "color"             : '#ffffff',
                "border-color"      : '#000000',
                "border-width"      : '1px',
                "border-style"      : 'solid',
                "border-radius"     : '15px',
                "background-color"  : '#111111'
            },
            "functions" : {
                "display"           : 'inline-block',
                "position"          : 'absolute',
                "top"               : '0',
                "float"             : 'right',
                "width"             : '24%',
                "height"            : '100%',
                "color"             : '#000000',
                "border-color"      : '#800000',
                "border-size"       : '2px',
                "background-color"  : '#ffaa00',
                "min-height"        : '60px',
                "vertical-align"    : 'middle',
                "text-align"        : 'center'
            },
            "function"  : {
                "display"           : 'block',
                "width"             : '50%',
                "min-height"        : '35px',
                "margin"            : '9px auto',
                "padding"           : '6px 0 0 0',
                "color"             : '#000000',
                "font-weight"       : 'bold',
                "border-color"      : '#800000',
                "border-size"       : '1px',
                "background-color"  : '#eeffbb',
                "border-radius"     : '15px'
            },
            "_0" : {
                "width"             : '54%'
            },
            "C" : {
                "color"             : 'ff8800',
                "background-color"  : '#ff0000',
                "font-weight"       : 'bold'
            }
        },
        "screen_default_value" : '0.00',
        "default_decimal"      : 2
    };
    
    /* This will contain the elements that combine to make the calculator. */
    this.elements = {};
    
    /* Might need a cache. */
    this.cache    = {};
    
    /* For folks that like to see some version control. */
    this.version  = 1.01;
    
    /* Stores the applied Button id's */
    this.buttons = {"memory": 0, "button": 0, "function": 0};
    
    /* The DOM element in the page where the Calculator will added */
    this.app = 'app';
    
};

/**
 * Reusable global method to set an attribute.
 *
 * @var element The element which the attribute will be set on
 * @var attr    The attribute that will be set
 * @var value   The value that will be aplied to the attibute
 */
$calculator.prototype.setAttribute = function (element, attr, value) {
    "use strict";
    console.log("setting attribute '" + attr + "' with value '" + value + "' for '" + element + "'");
    this.elements[element].setAttribute(attr, value);
};

/**
 * Reusable global method to check if current element is a child of another.
 * Returns the parent element if a match is found.
 *
 * @var element  This is the child element for which we are searching for the parent
 */
$calculator.prototype.getParent = function (element) {
    "use strict";
    var x, obj, children, child;
    for (x in this.config.containers) {
        console.log("searching: " + x);
        obj      = this.config.containers[x];
        children = obj.children;
        for (child in children) {
            console.log("checking child: " + children[child]);
            if (children[child] === element) {
                // return x as parent
                console.log("child: '" + children[child] + "' === element: '" + element);
                return x;
            }
        }
    }
};

/**
 * Creates DOM elements and attaches them to the defined element object -
 * or returns the lement of no target is defined
 *
 * @var type    The type of element to be created
 * @var target  The target element object (optional)
 */
$calculator.prototype.createElement = function (type, target) {
    "use strict";
    console.log("ceating element: '" + type + "' for target: '" + target);
    var self = this, elm;
    if (type) {
        elm = window.document.createElement(type);
    }
    if (target) {
        self.elements[target] = elm;
    } else {
        return elm;
    }
};

/**
 * Sets up the Memory buttons.
 *
 * @var calc  The type of calulator being rendered
 * @var count The number of element to be created
 * @var type  The type of element to be created
 */
$calculator.prototype.setMemories = function (calc, count, type) {
    "use strict";
    var i, elm, buttons, arr, id, output = [], text, ids = [];
    console.log("button creation memory: "  + calc + " - " + count + " - " + type);
    // Define the buttun variations based on typ calculator type.
    buttons = { "standard" : ['MR', 'MC', 'M+', 'M-', 'MF', 'MU']};
    // get the buttons according to the specified calc value
    arr = buttons[calc];
    for (i = 0; i < count; i = i + 1) {
        text = arr[i];
        elm = this.createElement(type);
        elm.innerHTML = text; // Add the buttons` text value from the array
        // Set the DOM objects 'id' attribute.
        elm.setAttribute('id', text);
        ids.push(text);
        output.push(elm);
    }
    // create a button group in the ID container
    this.buttons.memory = ids;
    return output;
};

/**
 * Sets up the Primary Calculator buttons.
 *
 * @var calc  The type of calulator being rendered
 * @var count The number of element to be created
 * @var type  The type of element to be created
 */
$calculator.prototype.setButtons = function (calc, count, type) {
    "use strict";
    var i, elm, buttons, arr, id, output = [], text, ids = [];
    console.log("button creation button: " + calc + " - " + count + " - " + type);
    // Define the buttun variations based on typ calculator type.
    buttons = { "standard" : ['9', '8', '7', '6', '5', '4', '3', '2', '1', '0', 'C', '=']};
    // get the buttons according to the specified calc value
    arr = buttons[calc];
    for (i = 0; i < count; i = i + 1) {
        text = arr[i];
        elm = this.createElement(type);
        elm.innerHTML = text; // Add the buttons` text value from the array
        // Set the DOM objects 'id' attribute.
        // the DOM does not like numeric values as element ID's
        if (text >= 0 && text <= 9) {
            id = "_" + text;
        } else {
            id = text;
        }
        elm.setAttribute('id', id);
        ids.push(id);
        output.push(elm);
    }
    // create a button group in the ID container
    this.buttons.button = ids;
    return output;
};

/**
 * Sets up the Primary Function buttons.
 *
 * @var calc  The type of calulator being rendered
 * @var count The number of element to be created
 * @var type  The type of element to be created
 */
$calculator.prototype.setFunctions = function(calc, count, type) {
    "use strict";
    var i, elm, buttons, arr, id, output = [], text, ids = [];
    console.log("button creation function: " + calc + " - " + count + " - " + type);
    // Define the buttun variations based on typ calculator type.
    buttons = { "standard" : ['+', '-', 'x', '/', '=']};
    // get the buttons according to the specified calc value
    arr = buttons[calc];
    for (i = 0; i < count; i = i + 1) {
        text = arr[i];
        console.log("creation function: " + arr[i]);
        elm = this.createElement(type);
        elm.innerHTML = text; // Add the buttons` text value from the array
        // Set the DOM objects 'id' attribute.
        elm.setAttribute('id', text);
        ids.push(text);
        output.push(elm);
    }
    // create a button group in the ID container
    this.buttons.function = ids;
    // return output
    return output;
};

/**
 * Creates the Calculator object with all container elements.
 */
$calculator.prototype.create = function () {
    "use strict";
    var self = this, index = 0, i, x, root = null, elm, obj, level, children, parent, func, elements;
    
    /* Reusable private function to create elements. Uses the global method to complete task. */
    function createElement(self, element, type) {
        self.createElement(type, element);
    }
    
    /* Reusable private function to append an object to an element.. */
    function appendChild(element, object) {
        self.elements[element].appendChild(object);
    }
    
    /* Set the 'type' of calculator - use 'standard; as the default */
    this.type = 'standard';
    
    /* Lets check the version */
    console.log("version: " + this.version);
    
    /* Iterate through the container configuration. */
    for (x in this.config.containers) {
        if (x.length >= 1 && this.config.containers[x]) {
            // Make sure this is null for each loop
            parent = null;
            
            // Lets save a reference to the 'root' element. x is the 'current element' or continer
            if (index === 0) { root = x; }
            console.log(x);
            
            // Object contains information on level of container and any children.
            obj   = this.config.containers[x];
            console.log("type: " + obj.type); // show the type value
            
            // Get the elements level.
            level = obj.level;
            console.log("level: " + level); // show the level
            
            // Create the elements DOM object.
            if (obj.count >= 1 && obj.children.length == 0) {
                func = obj.function;
                console.log("creating button group with function: " + func);
                this.elementArray = true;
                this.elementArrayData = this[func](this.type, obj.count, obj.type);
            } else {
                createElement(self, x, obj.type);
                // Set the DOM objects 'id' attribute.
                this.setAttribute(x, 'id', x);
                this.elementArray = false;
            }
            console.log("element array: " + this.elementArray);
            
            // If level is greater than zero get the elements parent.
            if (level > 0) {
                // Get the parent of the current element.
                console.log("checking parent for: " + x);
                parent = this.getParent(x);
            }
            if (parent) {
                // We have parent, lets append this element object to the parent.
                console.log("found parent: " + parent + " for: " + x);
                if (this.elementArray == true) {
                    console.log("loading elements array...");
                    elements = this.elementArrayData;//this.elements[x];
                    if (elements.length >= 1) {
                        for (i in elements) {
                            console.log("appending element buttons: " + elements[i]);
                            appendChild(parent, elements[i]);
                        }
                    } else {
                        // error??
                        console.log("elements length: " + elements.length);
                    }
                    
                    
                } else {
                    appendChild(parent, this.elements[x]);
                }
            }
            
            /* Increment the index. */
            index = (index + 1);
        }
    }
    return root;
};

$calculator.prototype.setStyles = function () {
    "use strict";
    var x, y, object, value, arr, i, obj, z, v;
    // iterate the style config
    for (x in this.config.styles) {
        console.log("styles x: " + x);
        if (x.length) {
            for (y in this.config.styles[x]) {
                console.log("styles y: " + y);
                object = this.config.styles[x];
                value = object[y];
                console.log("style value: " + value);
                if (document.getElementById(x)) {
                    document.getElementById(x).style[y] = value;
                } else {
                    console.log("alternative style method applied for: " + x);
                    arr = this.buttons[x];
                    console.log("alternative object: " + arr);
                    for (i in arr) {
         //               console.log("alternative i: " + i);
         //              console.log("alternative arr: " + arr[i]);
                        v = arr[i];
                            console.log("alternative id: " + v);
                            if (document.getElementById(v)) {
                                document.getElementById(v).style[y] = value;
                            } else {
                                console.log("alternative failed for id: " + v + " -style: " + y + " -value: " + value );
                            }
                 //       }
                    }
                }
            }
        }
    }
};

$calculator.prototype.init = function () {
    "use strict";
    
    // Create the Calculator within the DOM
    var container = this.create();
    
    // Adding the Calculator to the HTML page inside the specified element
    document.getElementById(this.app).appendChild(this.elements[container]);
    this.setStyles();
    
    
};