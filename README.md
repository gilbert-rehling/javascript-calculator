# README for JavaScript Calculator #

This JavaScript Calculator project was created to use a learning tool for anyone wishing to learn some native JS prototyping.

It uses a JSON formatted configuration which stores the configuration that is used to build the calculator.
The goal is to be able to store multiple variations of calculators that will use a URL parameter to set the requested calculator type.
Currently there is only the 'default' configuration available.

The create method uses the configuration the build a nested DOM object that build the calculator from the outside in.
Several of the internal methods also store configs which would ideally be moved to the initialising function as well.

**Version: 1.0 beta**

This calculator can be run on a local filesystem as its all front end code. All the styles for the calculator are stored in the configuration.
Simply place the files into a folder and open the index.html in a browser.

There is still a lot on 'console logging' active, primarily to help the JS students to understand the order in which the processes are triggered.
The HTML file inits the whole process.

The next iteration (stage 2) of the project will be the coding of the mathematical functions.

Feel free to copy and modify to your hearts content.

**Author Website:** [www.gilbert-rehling.com](http://www.gilbert-rehling.com)

Copyright (c) 2017 Gilbert Rehling (no rights reserved)